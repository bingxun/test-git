#v5
#test git pull

xmlStr=$(cat /tmp/fortify-report.xml)
# Count identity nodes
nodeCount=$(echo "$xmlStr" | xmllint --xpath "count(/ReportDefinition/ReportSection[1]/SubSection[2]/IssueListing/Chart/GroupingSection)" -)
# Iterate the nodeset by index
for i in $(seq 1 $nodeCount);do
   #echo "$xmlStr" | xmllint --xpath "concat(/Chart/GroupingSection[$i]/groupTitle,"  " /Chart/GroupingSection[$i]/@count)" - ; echo
  TEST1=$(echo "$xmlStr" | xmllint --xpath "string(/ReportDefinition/ReportSection[1]/SubSection[2]/IssueListing/Chart/GroupingSection[$i]/groupTitle)" - ; echo)
  TEST2=$(echo "$xmlStr" | xmllint --xpath "string(/ReportDefinition/ReportSection[2]/SubSection[2]/IssueListing/Chart/GroupingSection[$i]/@count)" - ; echo)
  TEST3=$TEST1" "---" "$TEST2
  echo "Severity:" $TEST3
done
